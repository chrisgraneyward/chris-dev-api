#!/bin/bash

if [ "$1" == "start" ]
then
  if [ "$2" == "dev" ]
  then
    docker compose down -v
    docker compose up --build -d
  elif [ "$2" == "nogpu" ]
  then
    docker compose -f docker-compose.prod.nogpu.yml down -v
    docker compose -f docker-compose.prod.nogpu.yml up --build -d
  else
    docker compose -f docker-compose.prod.yml down -v
    docker compose -f docker-compose.prod.yml up --build -d
  fi
elif [ "$1" == "restart" ]
then
  if [ "$2" == "dev" ]
  then
    docker compose up --build -d
  elif [ "$2" == "nogpu" ]
  then
    docker compose -f docker-compose.prod.nogpu.yml up --build -d --force-recreate
  else
    docker compose -f docker-compose.prod.yml up --build -d --force-recreate
  fi
elif [ "$1" == "stop" ]
then
    docker compose down -v --remove-orphans
elif [ "$1" == "logs" ]
then
    docker compose logs -f
fi
