from typing import List
from urllib import response
import ktrain
import numpy as np
from fastapi import FastAPI, HTTPException, Depends, Request
from fastapi.routing import APIRoute
from fastapi.responses import HTMLResponse, Response, RedirectResponse
import re
import requests
from wordcloud import WordCloud, STOPWORDS
from .response_models import HatePrediction, HatePredictionBatch, PerspectiveRegression, PerspectiveRegressionBatch
from fastapi.middleware.cors import CORSMiddleware
import joblib
import os

hate_predictor = ktrain.load_predictor('cbad/models/cbad')
toxicity_regressor = joblib.load('cbad/models/perspective/toxicity.pkl')
severe_toxicity_regressor = joblib.load('cbad/models/perspective/toxicity.pkl')
sexually_explicit_regressor = joblib.load('cbad/models/perspective/sexually_explicit.pkl')
obscene_regressor = joblib.load('cbad/models/perspective/obscene.pkl')
inflammatory_regressor = joblib.load('cbad/models/perspective/inflammatory.pkl')
identity_attack_regressor = joblib.load('cbad/models/perspective/identity_attack.pkl')
insult_regressor = joblib.load('cbad/models/perspective/insult.pkl')
threat_regressor = joblib.load('cbad/models/perspective/threat.pkl')
profanity_regressor = joblib.load('cbad/models/perspective/profanity.pkl')

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=['*'],  # 'rapidapi.com', 'cbad.p.rapidapi.com'
    allow_credentials=False,
    allow_methods=['*'],
    allow_headers=['*']
)


def verify_rapidapi(req: Request):
    if os.getenv('ENV') == 'develop':
        return True
    if 'X-RapidAPI-Proxy-Secret' not in req.headers:
        raise HTTPException(
            status_code=401,
            detail='Unauthorized, please use rapidapi https://rapidapi.com/chrisgraneyward-Cf0g1vby6xt/api/cbad'
        )
    token = req.headers['X-RapidAPI-Proxy-Secret']
    if token != os.getenv('RAPIDAPI_PROXY_SECRET'):
        raise HTTPException(
            status_code=401,
            detail='Unauthorized'
        )
    return True


def hate_preprocess(texts):
    texts = list(map(str.lower, texts))
    texts = [
        re.sub('(https:|http:|www\.)\S*', 'http', re.sub('@\w{0,15}', '@user', text)) for text in texts
    ]
    return texts


def get_preds(texts):
    if len(texts) == 0:
        return []
    texts = hate_preprocess(texts)
    probas = hate_predictor.predict(texts, return_proba=True)
    predictions = hate_predictor.get_classes()

    output = [
        {'isHate': predictions[np.argmax(probas[i])], 'probability': float(max(probas[i])), 'original': texts[i]}
        for i in range(len(probas))
    ]

    return output


def get_recent_tweets(query):
    url = f"https://api.twitter.com/2/tweets/search/recent?query={query}"
    headers = {
        "Authorization": "Bearer " + "AAAAAAAAAAAAAAAAAAAAAGQYKAEAAAAA6vRXqs2tOH8g5eh60ly6BylLSNs%3DKFLremcqqOAJphVixjMFvnVMPo6EjUnW3ofSxc8tLzlAEQX0I0"
    }
    res = requests.request("GET", url, headers=headers).json()
    if 'data' not in res:
        if 'status' in res and 'detail' in res:
            raise HTTPException(
                status_code=res['status'],
                detail='Forwarded Twitter error: ' + res['detail']
            )
        if 'errors' in res:
            raise HTTPException(
                status_code=422,
                detail=res['errors'][0]['message']
            )
        if res['meta']['result_count'] == 0:
            raise HTTPException(
                status_code=404,
                detail='User or hashtag does not exist.'
            )

    return get_preds(list(map(map_clean, res['data'])))


def map_clean(tweet):
    return str(tweet["text"]) \
        .replace('\n', ' ') \
        .replace('\u2019', "'") \
        .replace('&amp;', '&') \
        .replace('\u2026', ' ...')


@app.api_route('/predict/hate', methods=['GET'], response_model=HatePrediction)
async def single_hate_prediction(text: str, authorise: bool = Depends(verify_rapidapi)):
    return get_preds([text])[0]


@app.api_route('/predict/hate/batch', methods=['POST'], response_model=HatePredictionBatch)
async def batch_hate_prediction(texts: List[str], authorise: bool = Depends(verify_rapidapi)):
    return get_preds(texts)


@app.api_route('/predict/hate/twitter/user/{username}', methods=['GET'], response_model=HatePredictionBatch)
async def hate_prediction_twitter_account(username: str, authorise: bool = Depends(verify_rapidapi)):
    return get_recent_tweets("from:" + username)


@app.api_route('/predict/hate/twitter/hashtag/{hashtag}', methods=['GET'], response_model=HatePredictionBatch)
async def hate_prediction_twitter_hashtag(hashtag: str, authorise: bool = Depends(verify_rapidapi)):
    return get_recent_tweets("lang%3Aen%20%23" + hashtag)


@app.api_route('/predict/hate/explain', methods=['GET'])
async def explain_hate_prediction(text: str, authorise: bool = Depends(verify_rapidapi)):
    if len(text.split(' ')) <= 1:
        return HTMLResponse('<div>Explained predictions must have at least two words.</div>')
    return HTMLResponse(content=hate_predictor.explain(text).data, status_code=200)


@app.api_route(
    '/visual/wordcloud',
    methods=['GET'],
    responses={
        200: {
            "content": {"image/png": {}}
        }
    },
    response_class=Response
)
async def generate_wordcloud(text: str):
    text = text.lower()
    wordcloud = WordCloud(
        width=3000,
        height=2000,
        max_words=1000,
        random_state=1,
        background_color='white',
        colormap='Dark2_r',
        collocations=False,
        stopwords=STOPWORDS
    ).generate(text)

    return Response(content=wordcloud.to_svg(), media_type="image/svg+xml")


@app.api_route('/quantify/perspective', methods=['GET'], response_model=PerspectiveRegression)
async def single_toxicity_regression(text: str, authorise: bool = Depends(verify_rapidapi)):
    return {
        'toxicity': toxicity_regressor.predict([text])[0],
        'severe_toxicity': severe_toxicity_regressor.predict([text])[0],
        'sexually_explicit': sexually_explicit_regressor.predict([text])[0],
        'obscene': obscene_regressor.predict([text])[0],
        'inflammatory': inflammatory_regressor.predict([text])[0],
        'identity_attack': identity_attack_regressor.predict([text])[0],
        'insult': insult_regressor.predict([text])[0],
        'threat': threat_regressor.predict([text])[0],
        'profanity': profanity_regressor.predict([text])[0],
        'original': text
    }


@app.api_route('/quantify/perspective/batch', methods=['POST'], response_model=PerspectiveRegressionBatch)
async def batch_toxicity_regression(texts: List[str], authorise: bool = Depends(verify_rapidapi)):
    if len(texts) == 0:
        return []
    return [{
        'toxicity': float(toxicity),
        'severe_toxicity': float(severe_toxicity),
        'sexually_explicit': float(sexually_explicit),
        'obscene': float(obscene),
        'inflammatory': float(inflammatory),
        'identity_attack': float(identity_attack),
        'insult': float(insult),
        'threat': float(threat),
        'profanity': float(profanity),
        'original': text
    } for text, toxicity, severe_toxicity, sexually_explicit, obscene, inflammatory, identity_attack, insult, threat, profanity in zip(
        texts,
        toxicity_regressor.predict(texts),
        severe_toxicity_regressor.predict(texts),
        sexually_explicit_regressor.predict(texts),
        obscene_regressor.predict(texts),
        inflammatory_regressor.predict(texts),
        identity_attack_regressor.predict(texts),
        insult_regressor.predict(texts),
        threat_regressor.predict(texts),
        profanity_regressor.predict(texts)
    )]


@app.get('/', include_in_schema=False)
async def redircet_to_documentation():
    return RedirectResponse(url='/docs')


@app.get('/health', status_code=200, include_in_schema=False)
async def healthcheck():
    return {'detail': 'Ok'}


def use_route_names_as_operation_ids(app: FastAPI) -> None:
    """
    Simplify operation IDs so that generated API clients have simpler function
    names.

    Should be called only after all routes have been added.
    """
    for route in app.routes:
        if isinstance(route, APIRoute):
            route.operation_id = route.name


use_route_names_as_operation_ids(app)
