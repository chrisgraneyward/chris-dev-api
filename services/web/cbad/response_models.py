from pydantic import BaseModel
from typing import List


class HatePrediction(BaseModel):
    isHate: str
    probability: float
    original: str


class PerspectiveRegression(BaseModel):
    toxicity: float
    severe_toxicity: float
    sexually_explicit: float
    obscene: float
    inflammatory: float
    identity_attack: float
    insult: float
    threat: float
    profanity: float
    original: str


HatePredictionBatch = List[HatePrediction]
PerspectiveRegressionBatch = List[PerspectiveRegression]
