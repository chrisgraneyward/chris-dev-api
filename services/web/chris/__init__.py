import joblib
import numpy as np
from fastapi import FastAPI
from .response_models import PlotPrediction
from fastapi.middleware.cors import CORSMiddleware

plot_predictor = joblib.load('chris/model/demonstration.gz')

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=False,
    allow_methods=["*"],
    allow_headers=["*"]
)


@app.get('/predict/plot', response_model=PlotPrediction)
async def film_plot_prediction(text: str):
    probas = plot_predictor.predict_proba([text])
    genres = plot_predictor.named_steps['clf'].classes_
    max_index = np.argmax(probas)
    pred, accuracy = genres[max_index], probas[0][max_index]
    pred = pred.tolist()

    return {'genre_prediction': pred, 'accuracy': accuracy}
