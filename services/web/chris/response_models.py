from pydantic import BaseModel
from typing import List


class PlotPrediction(BaseModel):
    genre_prediction: str
    accuracy: float
