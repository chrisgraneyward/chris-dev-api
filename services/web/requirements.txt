fastapi[all]
fastapi_utils
pydantic
hypercorn[h3]
ktrain==0.25.4
joblib
wordcloud
git+https://github.com/amaiya/eli5@tfkeras_0_10_1